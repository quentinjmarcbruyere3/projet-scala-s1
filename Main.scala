import scala.collection.immutable.ArraySeq
import scala.io.Source

/**
 * Main app containg program loop
 */
object Main extends App {

  println("Starting application")

  val status = run()

  println("\nExiting application")
  println(s"Final status: ${status.message}")

  /**
   * Read action from Stdin and execute it
   * Exit if action is 'exit' or if an error occured (status > 0)
   * DO NOT MODIFY THIS FUNCTION
   */
  def run(canvas: Canvas = Canvas()): Status = {
    println("\n======\nCanvas:")
    canvas.display

    print("\nAction: ")

    val action = scala.io.StdIn.readLine()

    val (newCanvas, status) = execute(ArraySeq.unsafeWrapArray(action.split(' ')), canvas)

    if (status.error) {
      println(s"ERROR: ${status.message}")
    }

    if (status.exit) {
      status 
    } else {
      run(newCanvas)  
    }
  }

  /**
   * Execute various actions depending on an action command and optionnaly a Canvas
   */
  def execute(action: Seq[String], canvas: Canvas): (Canvas, Status) = {
    val execution: (Seq[String], Canvas) => (Canvas, Status) = action.head match {
      case "exit" => Canvas.exit
      case "dummy" => Canvas.dummy
      case "dummy2" => Canvas.dummy2
      case "new_canvas" => Canvas.newCanvas
      case "update_pixel" => Canvas.updatePixel
      case "draw" => Canvas.draw
      case "load_image" => Canvas.load_image
      case _ => Canvas.default
      // TODO: Add command here
    }

    execution(action.tail, canvas)
  }
}

/**
 * Define the status of the previous execution
 */
case class Status(
  exit: Boolean = false,
  error: Boolean = false,
  message: String = ""
)

/**
 * A pixel is defined by its coordinates along with its color as a char
 */
case class Pixel(x: Int, y: Int, color: Char = ' ') {
  override def toString(): String = {
    color.toString
  }
}

/**
 * Companion object of Pixel case class
 */
object Pixel {
  /**
   * Create a Pixel from a string "x,y"
   */
  def apply(s: String): Pixel = {
    val tabCoordonee = s.split(",")
    Pixel(tabCoordonee(0).toInt, tabCoordonee(1).toInt)
  }

  /**
   * Create a Pixel from a string "x,y" and a color 
   */
  def apply(s: String, color: Char): Pixel = {
    val tabCoordonee = s.split(",")
    Pixel(tabCoordonee(0).toInt, tabCoordonee(1).toInt, color)
  }
}

/**
 * A Canvas is defined by its width and height, and a matrix of Pixel
 */
case class Canvas(width: Int = 0, height: Int = 0, pixels: Vector[Vector[Pixel]] = Vector()) {

  /**
   * Print the canvas in the console
   */
  def display: Unit = {
    if (pixels.size == 0) {
      println("Empty Canvas")
    } else {
      println(s"Size: $width x $height")
      pixels.map(ligne => {
        ligne.map(pixel => {
          print(pixel)
        })
        println()
      })
    }
  }

  /**
   * Takes a pixel in argument and put it in the canvas
   * in the right position with its color
   */
  def update(pixel: Pixel): Canvas = {
    
    val newPixels = pixels.updated(pixel.y, pixels(pixel.y).updated(pixel.x, Pixel(pixel.y, pixel.x, pixel.color)))

    this.copy(pixels = newPixels)
  }

  /**
   * Return a Canvas containing all modifications
   */
  def updates(pixels: Seq[Pixel]): Canvas = {
    pixels.foldLeft(this)((f, p) => f.update(p))
  }

  // TODO: Add any useful method
}

/**
 * Companion object for Canvas case class
 */
object Canvas {
  /**
   * Exit execution
   */
  def exit(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = 
    (canvas, Status(exit = true, message="Received exit signal"))

  /**
   * Default execution for unknown action
   */
  def default(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = 
    (canvas, Status(error = true, message = s"Unknown command"))

  /**
   * Create a static Canvas
   */
  def dummy(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = 
    if (arguments.size > 0) 
      (canvas, Status(error = true, message = "dummy action does not excpect arguments"))
    else  {
      val dummyCanvas = Canvas(
        width = 3,
        height = 4,
        pixels = Vector(
          Vector(Pixel(0, 0, '#'), Pixel(1, 0, '.'), Pixel(2, 0, '#')),
          Vector(Pixel(0, 1, '#'), Pixel(1, 1, '.'), Pixel(2, 1, '#')),
          Vector(Pixel(0, 2, '#'), Pixel(1, 2, '.'), Pixel(2, 2, '#')),
          Vector(Pixel(0, 3, '#'), Pixel(1, 3, '.'), Pixel(2, 3, '#'))
        )
      )
      
      (dummyCanvas, Status())
    }

  /**
   * Create a static canvas using the Pixel companion object
   */
  def dummy2(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = 
    if (arguments.size > 0) 
      (canvas, Status(error = true, message = "dummy action does not excpect arguments"))
    else  {
      val dummyCanvas = Canvas(
        width = 3,
        height = 1,
        pixels = Vector(
          Vector(Pixel("0,0", '#'), Pixel("1,0"), Pixel("2,0", '#'))
        )
      )
      
      (dummyCanvas, Status())
    }

  /**
   *
   * Create canvas with différents size and charactèrs
   *
   * @param arguments Width Heigth Character
   * @param canvas Canvas use
   * @return Canva need to be print
   */
  def newCanvas(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 3)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 3)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      val newCanvas = Canvas(
        width = arguments(0).toInt,
        height = arguments(1).toInt,
        pixels = Vector.fill(arguments(1).toInt,arguments(0).toInt)(Pixel("0,0",arguments(2).charAt(0)))
      )
      (newCanvas, Status())
    }
  }

  /**
   *
   * Update one pixel in the canvas
   *
   * @param arguments Pixel coordination and color
   * @param canvas Canvas use
   * @return Canva need to be print
   */
  def updatePixel(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 2)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 2)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      if (arguments(0).split(",")(0).toInt < 0) {
        (canvas, Status(error = true, message = "Width too short"))
      } else if (arguments(0).split(",")(0).toInt >= canvas.width) {
        (canvas, Status(error = true, message = "Width too long"))
      } else if (arguments(0).split(",")(1).toInt < 0) {
        (canvas, Status(error = true, message = "Height too short"))
      } else if (arguments(0).split(",")(1).toInt >= canvas.height) {
        (canvas, Status(error = true, message = "Height too long"))
      } else {
        val newCanvas = canvas.update(Pixel(arguments(0), arguments(1).charAt(0)))
        (newCanvas, Status())
      }
    }
  }

  /**
   *
   * Contain all draw function
   *
   * @param arguments Function use in draw
   * @param canvas Canvas use
   * @return Function choose
   */

  def draw(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    val execution: (Seq[String], Canvas) => (Canvas, Status) = arguments.head match {
      case "fill" => Canvas.fill
      case "line" => Canvas.line
      case "linev2" => Canvas.linev2
      case "rectangle" => Canvas.rectangle
      case "linev3" => Canvas.linev3
      case _ => Canvas.default
    }

    execution(arguments.tail, canvas)
  }

  /**
   *
   * Draw line v3 on the canvas
   *
   * @param arguments Coords for first and last pixel
   * @param canvas    Canvas to use
   * @return Canvas need to be print
   */
  def linev3(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 3)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 3)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      val pixel1 = arguments(0)
      val pixel2 = arguments(1)
      if (pixel1.split(",")(0).toInt < 0 || pixel2.split(",")(0).toInt < 0)
        (canvas, Status(error = true, message = "Width too short"))
      else if (pixel1.split(",")(0).toInt >= canvas.width || pixel2.split(",")(0).toInt >= canvas.width)
        (canvas, Status(error = true, message = "Width too long"))
      else if (pixel1.split(",")(1).toInt < 0 || pixel2.split(",")(1).toInt < 0)
        (canvas, Status(error = true, message = "Height too short"))
      else if (pixel1.split(",")(1).toInt >= canvas.height || pixel2.split(",")(1).toInt >= canvas.height)
        (canvas, Status(error = true, message = "Height too long"))
      else {
        val dx = pixel2.split(",")(0).toInt - pixel1.split(",")(0).toInt
        val dy = pixel2.split(",")(1).toInt - pixel1.split(",")(1).toInt
        val D = (2 * dy) - dx

        if ((pixel2.split(",")(1).toInt - pixel1.split(",")(1).toInt).abs < (pixel2.split(",")(0).toInt - pixel1.split(",")(0).toInt).abs) {
          if (pixel1.split(",")(0).toInt > pixel2.split(",")(0).toInt) {
            Canvas.drawlinev3X(Seq(arguments(1), arguments(0), arguments(2)), canvas, D, -dy, -dx)
          } else {
            Canvas.drawlinev3X(arguments, canvas, D, -dy, dx)
          }
        } else {
          if (pixel1.split(",")(1).toInt > pixel2.split(",")(1).toInt) {
            Canvas.drawlinev3Y(Seq(arguments(1), arguments(0), arguments(2)), canvas, D, dy, -dx)
          } else {
            Canvas.drawlinev3Y(arguments, canvas, D, dy, -dx)
          }
        }
      }
    }
  }

  /**
   *
   * Recursive function for draw line v3 on X
   *
   * @param arguments Posistion pixel 1, pixel 2 and color
   * @param canvas    Canvas to use
   * @param D         D already calculated
   * @param dy        dy value
   * @param dx        dx value
   * @return Canvas to be print
   */
  def drawlinev3X(arguments: Seq[String], canvas: Canvas, D: Int, dy: Int, dx: Int): (Canvas, Status) = {
    val pixel1 = arguments(0)
    val pixel2 = arguments(1)

    var newCanvas = canvas.update(Pixel(pixel1, arguments(2).charAt(0)))

    if (pixel1.split(",")(0).toInt < pixel2.split(",")(0).toInt) {
      if (D > 0) {
        val newD = D + (2 * (dy - dx))
        if (dy < 0) {
          newCanvas = Canvas.drawlinev3X(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + (pixel1.split(",")(1).toInt - 1).toString, pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
        } else {
          newCanvas = Canvas.drawlinev3X(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + (pixel1.split(",")(1).toInt + 1).toString, pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
        }
      } else {
        val newD = D + (2 * dy)
        newCanvas = Canvas.drawlinev3X(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + pixel1.split(",")(1), pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
      }
    }
    (newCanvas, Status())
  }

  /**
   *
   * Recursive function for draw line v3 on Y
   *
   * @param arguments Posistion pixel 1, pixel 2 and color
   * @param canvas    Canvas to use
   * @param D         D already calculated
   * @param dy        dy value
   * @param dx        dx value
   * @return Canvas to be print
   */
  def drawlinev3Y(arguments: Seq[String], canvas: Canvas, D: Int, dy: Int, dx: Int): (Canvas, Status) = {
    val pixel1 = arguments(0)
    val pixel2 = arguments(1)

    var newCanvas = canvas.update(Pixel(pixel1, arguments(2).charAt(0)))

    if (pixel1.split(",")(1).toInt < pixel2.split(",")(1).toInt) {
      if (D > 0) {
        val newD = D + (2 * (dx - dy))
        if (dx < 0) {
          newCanvas = Canvas.drawlinev3Y(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + (pixel1.split(",")(1).toInt + 1).toString, pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
        } else {
          newCanvas = Canvas.drawlinev3Y(Seq((pixel1.split(",")(0).toInt - 1).toString + "," + (pixel1.split(",")(1).toInt + 1).toString, pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
        }
      } else {
        val newD = D + (2 * dx)
        newCanvas = Canvas.drawlinev3Y(Seq(pixel1.split(",")(0).toString + "," + (pixel1.split(",")(1).toInt + 1).toString, pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
      }
    }
    (newCanvas, Status())
  }

  /**
   *
   * Draw line v2 on the canvas
   *
   * @param arguments Coords for first and last pixel
   * @param canvas Canvas to use
   * @return Canvas need to be print
   */
  def linev2(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 3)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 3)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      val pixel1 = arguments(0)
      val pixel2 = arguments(1)
      if (pixel1.split(",")(0).toInt < 0 || pixel2.split(",")(0).toInt < 0)
        (canvas, Status(error = true, message = "Width too short"))
      else if (pixel1.split(",")(0).toInt >= canvas.width || pixel2.split(",")(0).toInt >= canvas.width)
        (canvas, Status(error = true, message = "Width too long"))
      else if (pixel1.split(",")(1).toInt < 0 || pixel2.split(",")(1).toInt < 0)
        (canvas, Status(error = true, message = "Height too short"))
      else if (pixel1.split(",")(1).toInt >= canvas.height || pixel2.split(",")(1).toInt >= canvas.height)
        (canvas, Status(error = true, message = "Height too long"))
      else {
        val dx = pixel2.split(",")(0).toInt - pixel1.split(",")(0).toInt
        val dy = pixel2.split(",")(1).toInt - pixel1.split(",")(1).toInt
        val D = (2 * dy) - dx

        Canvas.drawlinev2(arguments, canvas, D, dy, dx)
      }
    }
  }

  /**
   *
   * Recursive function for draw line v2
   *
   * @param arguments Posistion pixel 1, pixel 2 and color
   * @param canvas Canvas to use
   * @param D D already calculated
   * @param dy dy value
   * @param dx dx value
   * @return Canvas to be print
   */
  def drawlinev2(arguments: Seq[String], canvas: Canvas, D: Int, dy: Int, dx: Int): (Canvas, Status) = {
    val pixel1 = arguments(0)
    val pixel2 = arguments(1)

    var newCanvas = canvas.update(Pixel(pixel1, arguments(2).charAt(0)))

    if (pixel1.split(",")(0).toInt < pixel2.split(",")(0).toInt) {
      if (D > 0) {
        val newD = D + (2 * (dy - dx))
        newCanvas = Canvas.drawlinev2(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + (pixel1.split(",")(1).toInt + 1).toString, pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
      } else {
        val newD = D + (2*dy)
        newCanvas = Canvas.drawlinev2(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + pixel1.split(",")(1), pixel2, arguments(2)), newCanvas, newD, dy, dx)._1
      }
    }
    (newCanvas, Status())
  }

  /**
   * 
   * Draw line on the canvas
   * 
   * @param arguments Coords for first and last pixel
   * @param canvas Canvas to use
   * @return Canvas need to be print
   */
  def line(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 3)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 3)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      val pixel1 = arguments(0)
      val pixel2 = arguments(1)
      if (pixel1.split(",")(0).toInt < 0 || pixel2.split(",")(0).toInt < 0)
        (canvas, Status(error = true, message = "Width too short"))
      else if (pixel1.split(",")(0).toInt >= canvas.width || pixel2.split(",")(0).toInt >= canvas.width)
        (canvas, Status(error = true, message = "Width too long"))
      else if (pixel1.split(",")(1).toInt < 0 || pixel2.split(",")(1).toInt < 0)
        (canvas, Status(error = true, message = "Height too short"))
      else if (pixel1.split(",")(1).toInt >= canvas.height || pixel2.split(",")(1).toInt >= canvas.height)
        (canvas, Status(error = true, message = "Height too long"))
      else {
        var newCanvas = canvas.update(Pixel(pixel1, arguments(2).charAt(0)))

        if (pixel1.split(",")(0).toInt - pixel2.split(",")(0).toInt == 0 && pixel1.split(",")(1).toInt - pixel2.split(",")(1).toInt != 0) {
          if (pixel1.split(",")(1).toInt < pixel2.split(",")(1).toInt) {
            newCanvas = Canvas.line(Seq((pixel1.split(",")(0) + "," + (pixel1.split(",")(1).toInt + 1).toString), pixel2, arguments(2)), newCanvas)._1
          } else {
            newCanvas = Canvas.line(Seq((pixel1.split(",")(0) + "," + (pixel1.split(",")(1).toInt - 1).toString), pixel2, arguments(2)), newCanvas)._1
          }
        } else if (pixel1.split(",")(0).toInt - pixel2.split(",")(0).toInt != 0 && pixel1.split(",")(1).toInt - pixel2.split(",")(1).toInt == 0) {
          if (pixel1.split(",")(0).toInt < pixel2.split(",")(0).toInt) {
            newCanvas = Canvas.line(Seq((pixel1.split(",")(0).toInt + 1).toString + "," + pixel1.split(",")(1), pixel2, arguments(2)), newCanvas)._1
          } else {
            newCanvas = Canvas.line(Seq((pixel1.split(",")(0).toInt - 1).toString + "," + pixel1.split(",")(1), pixel2, arguments(2)), newCanvas)._1
          }
        }
        (newCanvas, Status())
      }
    }
  }

  /**
   *
   * Fill part on a canvas
   *
   * @param arguments Coord of the first point
   * @param canvas Canvas use
   * @return Canvas need to be print
   */
  def fill(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 2)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 2)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      if (arguments(0).split(",")(0).toInt < 0) {
        (canvas, Status(error = true, message = "Width too short"))
      } else if (arguments(0).split(",")(0).toInt >= canvas.width) {
        (canvas, Status(error = true, message = "Width too long"))
      } else if (arguments(0).split(",")(1).toInt < 0) {
        (canvas, Status(error = true, message = "Height too short"))
      } else if (arguments(0).split(",")(1).toInt >= canvas.height) {
        (canvas, Status(error = true, message = "Height too long"))
      } else {
        val previousPixel = canvas.pixels(arguments(0).split(",")(1).toInt)(arguments(0).split(",")(0).toInt).color
        val y = arguments(0).split(",")(1).toInt
        val x = arguments(0).split(",")(0).toInt
        var newCanvas = canvas.update(Pixel(arguments(0), arguments(1).charAt(0)))

        if (x + 1 < canvas.width && newCanvas.pixels(y)(x + 1).color.equals(previousPixel)) {
          newCanvas = Canvas.fill(Seq((x + 1).toString + "," + y.toString, arguments(1)), newCanvas)._1
        }
        if (x - 1 >= 0 && newCanvas.pixels(y)(x - 1).color.equals(previousPixel)) {
          newCanvas = Canvas.fill(Seq((x - 1).toString + "," + y.toString, arguments(1)), newCanvas)._1
        }
        if (y + 1 < canvas.height && newCanvas.pixels(y + 1)(x).color.equals(previousPixel)) {
          newCanvas = Canvas.fill(Seq(x.toString + "," + (y + 1).toString, arguments(1)), newCanvas)._1
        }
        if (y - 1 >= 0 && newCanvas.pixels(y - 1)(x).color.equals(previousPixel)) {
          newCanvas = Canvas.fill(Seq(x.toString + "," + (y - 1).toString, arguments(1)), newCanvas)._1
        }

        (newCanvas, Status())
      }
    }
  }
  // TODO: Add any useful method

  /**
   *
   * Load image from a file
   *
   * @param arguments Path to the file
   * @param canvas Canvas use
   * @return Canvas to be print
   */
  def load_image(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 1)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 1)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      val content: Vector[String] = Source.fromFile(arguments(0)).getLines().toVector
      val newPixels = content.zipWithIndex.map({ case (ligne, indexLigne) =>
        ligne.zipWithIndex.map({ case (elem, indexElem) =>
          new Pixel(indexElem, indexLigne, elem)
        }).toVector
      })
      val newCanvas = Canvas(
        width = newPixels(0).size,
        height = newPixels.size,
        pixels = newPixels
      )
      (newCanvas, Status())
    }
  }

  /**
   *
   * Draw rectangle on canvas
   *
   * @param arguments Coord pixel 1, pixel 2 and color
   * @param canvas Canvas to use
   * @return Canvas to be print
   */
  def rectangle(arguments: Seq[String], canvas: Canvas): (Canvas, Status) = {
    if (arguments.size < 3)
      (canvas, Status(error = true, message = "Missing some arguments"))
    else if (arguments.size > 3)
      (canvas, Status(error = true, message = "Too many arguments"))
    else {
      val pixel1 = arguments(0)
      val pixel2 = arguments(1)
      if (pixel1.split(",")(0).toInt < 0 || pixel2.split(",")(0).toInt < 0)
        (canvas, Status(error = true, message = "Width too short"))
      else if (pixel1.split(",")(0).toInt >= canvas.width || pixel2.split(",")(0).toInt >= canvas.width)
        (canvas, Status(error = true, message = "Width too long"))
      else if (pixel1.split(",")(1).toInt < 0 || pixel2.split(",")(1).toInt < 0)
        (canvas, Status(error = true, message = "Height too short"))
      else if (pixel1.split(",")(1).toInt >= canvas.height || pixel2.split(",")(1).toInt >= canvas.height)
        (canvas, Status(error = true, message = "Height too long"))
      else {
        val firstLine = Canvas.line(Seq(pixel1, pixel2.split(",")(0) + "," + pixel1.split(",")(1), arguments(2)), canvas)._1
        val secondLine = Canvas.line(Seq(pixel1.split(",")(0) + "," + pixel2.split(",")(1), pixel2, arguments(2)), firstLine)._1
        val thirdLine = Canvas.line(Seq(pixel1, pixel1.split(",")(0) + "," + pixel2.split(",")(1), arguments(2)), secondLine)._1
        val lastLine = Canvas.line(Seq(pixel2.split(",")(0) + "," + pixel1.split(",")(1), pixel2, arguments(2)), thirdLine)._1
        (lastLine, Status())
      }
    }
  }

}
